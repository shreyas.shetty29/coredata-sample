//
//  ViewController.swift
//  CoreData Sample
//
//  Created by Shreyas Shetty on 22/12/18.
//  Copyright © 2018 Shreyas Shetty. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    lazy var dataContainer: NSPersistentContainer = {
        return getHandleToDataContainer()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension ViewController {
    //MARK:- Create handle to PersistentContainer
    private func getHandleToDataContainer() -> NSPersistentContainer {
        let container = NSPersistentContainer(name: CoreDataConstants.dataModelName)
        container.loadPersistentStores { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Something went wrong \(error): \(error.description)")
            }
        }
        return container
    }
}

